<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo e(asset("css/main.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("css/app.css")); ?>">
    <!-- Scripts -->
    <?php echo app('Illuminate\Foundation\Vite')(['resources/sass/app.scss', 'resources/js/app.js']); ?>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <script src="<?php echo e(asset("js/menu.js")); ?>"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <?php
    //session_start();
    //header("url=route('home')");
    use App\Http\Controllers\HomeController;
    HomeController::generateCookie();
    //dd($_COOKIE)
    ?>
    <?php 
        use \App\Http\Controllers\BasketCollectionController;
        $cart = new BasketCollectionController();
        $quantityInCart = $cart->size();
    ?>
    <div id="app">
        <div class="navBarContainer">
            <div class="navBarLogo">
                <a href="<?php echo e(url('/')); ?>">
                    <!--  -->
                    <img src="<?php echo e(asset("svg/logo-no-background.svg")); ?>" width="175" height="35" />
                </a>
            </div>

            <div class="navBarMenu">
                <div class="megaMenu">
                    <span>Menu</span>
                    <div class="megaMenuContent">
                        <div class="megaMenuColumn">
                            <strong>&#127968; Return Home</strong>
                            <div class="megaMenuRow">
                                <a href="<?php echo e(route('main')); ?>">Go to Homepage</a>
                            </div>
                        </div>
                        <div class="megaMenuColumn">
                            <strong><i class=""><img src="<?php echo e(asset('png/icons8-online-store-30.png')); ?>" alt="" height="20" width="20"></i> View Our Products</strong>
                            <div class="megaMenuRow">
                                <a href="<?php echo e(route('products')); ?>">View All Products</a>
                            </div>
                            <div class="megaMenuRow">
                                <a href="<?php echo e(route('chairs')); ?>">View Chairs</a>
                            </div>
                            <div class="megaMenuRow">
                                <a href="<?php echo e(route('sofas')); ?>">View Sofas</a>
                            </div>
                            <div class="megaMenuRow">
                                <a href="<?php echo e(route('beds')); ?>">View Beds</a>
                            </div>
                            <div class="megaMenuRow">
                                <a href="<?php echo e(route('tables')); ?>">View Tables/Desks</a>
                            </div>
                        </div>
                        <div class="megaMenuColumn">
                            <strong><i class=""><img src="<?php echo e(asset('png/icons8-information-64.png')); ?>" alt="" width="20" height="20"></i> About TresorLDN</strong>
                            <div class="megaMenuRow">
                                <a href="<?php echo e(route('about')); ?>">About Us</a>
                            </div>
                            <div class="megaMenuRow">
                                <a href="<?php echo e(route('contact')); ?>">Contact Us</a>
                            </div>
                            <div class="megaMenuRow">
                                <a href="<?php echo e(route('review')); ?>">Review Us</a>
                            </div>

                            <div class="megaMenuRow">
                                <a href="<?php echo e(route('privacy')); ?>">Privacy Policy</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navBarSearch">
                <div class="searchBarContainer">
                    <form class="searchForm" method="POST" action="<?php echo e(route('search')); ?>">
                        <?php echo csrf_field(); ?>
                        <input type="text" placeholder="Enter a search" name="searchTarget" />
                        <button type="submit">Search</button>
                    </form>
                </div>
            </div>
            <div class="navBarLogin">
                <?php if(auth()->guard()->guest()): ?>
                <?php if(Route::has('login')): ?>
                <div class="navBarLoginOptions">
                    <a href="<?php echo e(route('login')); ?>">Login</a>
                </div>
                <?php endif; ?>

                <?php if(Route::has('register')): ?>
                <div class="navBarLoginOptions">
                    <a href="<?php echo e(route('register')); ?>">Register</a>
                </div>
                <?php endif; ?>
                <?php else: ?>
                <div class="navBarLoginOptions"><a href="<?php echo e(route('basket')); ?>"><i class=""><img src="<?php echo e(asset('png/icons8-buying-24.png')); ?>" width="20" height="20" alt=""></i> Basket (<?php echo e($quantityInCart); ?>)</a></div>
                <div class="navBarLoginOptions"><a href="<?php echo e(route('pastOrders')); ?>"><img src="<?php echo e(asset('png/icons8-cheque-50.png')); ?>" width="20" height="20"> Orders</a></div>
                <?php if(\Illuminate\Support\Facades\Auth::user()->isAdmin == 1): ?>
                <div class="navBarLoginOptions">
                    <a href="<?php echo e(route('admin')); ?>"><i class="fa"><img src="<?php echo e(asset('png/icons8-admin-settings-male-50.png')); ?>" alt="" width="20" height="20"></i> Admin</a>
                </div>
                <?php endif; ?>

                <div class="navBarLoginOption">
                    <i class="fa">&#xf007;</i> <?php echo e(Auth::user()->user_first_name); ?>

                    <div class="navBarUserSettings">
                        <a class="navBarUserSettings" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i style='font-size:16px' class=''><img src="<?php echo e(asset('png/icons8-logout-50.png')); ?>" width="20" height="20"></i> <?php echo e(__('Logout')); ?>

                        
                        </a>
                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                            <?php echo csrf_field(); ?>
                        </form>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            
            <div id="menuBtn" class="menuScreenBtn" onclick="showMenu()">
                <strong>&#9776;</strong>
            </div>
        </div>
    </div>
    
    <div id="menu" class="menuScreen">
    <div id="showSearch" class="menuScreenSearch">
        <div class="navBarSearch">
            <div class="searchBarContainer">
                <form class="searchForm" method="POST" action="<?php echo e(route('search')); ?>">
                    <?php echo csrf_field(); ?>
                    <input type="text" placeholder="Enter a search" name="searchTarget" />
                    <button type="submit">Search</button>
                </form>
            </div>
        </div>
    </div>
        <!-- <div class="megaMenuContent"> -->
        <div id="test" class="megaMenuColumn">
            <strong>&#127968; Return Home</strong>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('main')); ?>">Go to Homepage</a>
            </div>
        </div>
        
        <div class="megaMenuColumn">
            <strong><i class="fas"><img src="<?php echo e(asset('png/icons8-online-store-30.png')); ?>" alt="" height="20" width="20"></i> View Our Products</strong>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('products')); ?>">View All Products</a>
            </div>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('chairs')); ?>">View Chairs</a>
            </div>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('sofas')); ?>">View Sofas</a>
            </div>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('beds')); ?>">View Beds</a>
            </div>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('tables')); ?>">View Tables/Desks</a>
            </div>
        </div>
        <div class="megaMenuColumn">
            <strong><i class=""><img src="<?php echo e(asset('png/icons8-information-64.png')); ?>" alt="" height="20" width="20"></i> About TresorLDN</strong>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('about')); ?>">About Us</a>
            </div>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('contact')); ?>">Contact Us</a>
            </div>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('review')); ?>">Review Us</a>
            </div>
        </div>
        <div class="megaMenuColumn">
            <strong><i class="fas">&#xf05a;</i> Account</strong>
            <?php if(auth()->guard()->guest()): ?>
            <?php if(Route::has('login')): ?>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('login')); ?>">Login</a>
            </div>
            <?php endif; ?>

            <?php if(Route::has('register')): ?>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('register')); ?>">Register</a>
            </div>
            <?php endif; ?>
            <?php else: ?>
            <div class="megaMenuRow"><a href="<?php echo e(route('basket')); ?>"><i class="fa fa-shopping-cart"></i> Basket (<?php echo e($quantityInCart); ?>)</a></div>
            <div class="megaMenuRow"><a href="<?php echo e(route('pastOrders')); ?>"><img src="<?php echo e(asset('png/icons8-cheque-50.png')); ?>" alt="" height="20" width="20">Past Orders</a></div>
            <?php if(\Illuminate\Support\Facades\Auth::user()->isAdmin == 1): ?>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('admin')); ?>"><i class=""><img src="<?php echo e(asset('png/icons8-admin-settings-male-50.png')); ?>" alt="" height="20" width="20"></i> Admin</a>
            </div>
            <?php endif; ?>
            
            <div class="megaMenuRow">
                <i class="fa fa-user"></i><span> <?php echo e(Auth::user()->user_first_name); ?></span>
            </div>
            <div class="megaMenuRow">
                <a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                    <i style='font-size:16px' class='fas'><img src="<?php echo e(asset('png/icons8-logout-50.png')); ?>" alt="" height="20" width="20"></i> <?php echo e(__('Logout')); ?>

                
                </a>
                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                    <?php echo csrf_field(); ?>
                </form>
            </div>
            <?php endif; ?>
            <!-- <strong><i class="fas">&#xf05a;</i> About TresorLDN</strong>
                <div class="megaMenuRow">
                    <a href="<?php echo e(route('about')); ?>">About Us</a>
                </div>
                <div class="megaMenuRow">
                    <a href="<?php echo e(route('contact')); ?>">Contact Us</a>
                </div>
                <div class="megaMenuRow">
                    <a href="<?php echo e(route('review')); ?>">Review Us</a>
                </div> -->
        </div>
    </div>
    </div>
    </div>
    <main class="superContainer">
        <?php echo $__env->yieldContent('content'); ?>
    </main>
    </div>

    <footer class="footer">
        <br />
        <br />
        <span>
            <center>Copyright 2022 TresorLDN Limited Company</center>
        </span>
        <br />
        <span>
            <center>TresorLDN a limited company that advertises and sells various
                homeware products for all customers and businesses.
            </center>
        </span>
        <span>
            <center>Providing customer satisfaction
                by selling products that can be tailored to meet the customers needs.
            </center>
        </span>
        <br />
        <span>
            <center>Our website is powered by Laravel a PHP Framework created by <a href="https://github.com/taylorotwell">Taylor Otwell</a></center>
        </span>
        <br />
        <span>
            <center>
                <img src="<?php echo e(asset("svg/logo-no-background.svg")); ?>" width="250" height="35" />
            </center>
        </span>
        <br />
        <br />
        <br />
        <br />
    </footer>
</body>

</html><?php /**PATH C:\xampp\htdocs\tresorldn_e-commerce\resources\views/layouts/app.blade.php ENDPATH**/ ?>