<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use App\Models\Products;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Products::create(
            [
                'product_name' => 'MICKE Computer Desk',
                'product_type' => 'Table',
                'product_price' => 85,
                'product_description' => 'A clean and simple look that fits just about anywhere. You can combine it with other desks or drawer units in the MICKE series to extend your work space. The clever design at the back hides messy cables.',
                'created_at' => Date::today(),
                'image' => 'MICKE Computer Desk.jpg'
            ]
        );
        
        Products::create(
            [
                'product_name' => 'Three Seat Sofa',
                'product_type' => 'Sofa',
                'product_description' => 'Our beloved EKTORP seating has a timeless design and wonderfully thick, comfy cushions. The covers are easy to change, so buy an extra cover - or two, and change according to mood or season.',
                'product_price' => 499,
                'image' => 'Three Seat Sofa.jpg'
            ]
        );
        
        Products::create([
            'product_name' => 'Corner Flakarp Sofa',
            'product_type' => 'Sofa',
            'product_description' => 'Explore the luxurious sofa, bring the luxury to your home.',
            'product_price' => 295.50,
            'price_deduction' => 58.50,
            'image' => 'Corner Flakarp Sofa.jpg'
        ]);
        
        Products::create([
            'product_name' => 'Strandmon Recliner Nordvalla Dark Grey',
            'product_type' => 'Chair',
            'product_description' => 'A chair that is designed for comfort in your home and wherever you are.',
            'product_price' => 399.50,
            'price_deduction' => 75.50,
            'image' => 'Strandmon Recliner Nordvalla Dark Grey.jpg'
        ]);
        
        Products::create([
            'product_name' => 'Double Sized Upholstered bed, with two storage boxes, Kabusa light grey',
            'product_type' => 'Bed',
            'product_description' => 'Providing convenience to your hands.',
            'product_price' => 309.50,
            'price_deduction' => 32.50,
            'image' => 'Double Sized Upholstered Bed.jpg'
        ]);
        
        Products::create([
            'product_name' => 'Single Sized Reversible bed, white/pine',
            'product_type' => 'Bed',
            'product_description' => 'A bed with more customisations.',
            'product_price' => 150,
            'price_deduction' => 38.49,
            'image' => 'Single Sized Reversible Bed.jpg'
        ]);
        
        Products::create([
            'product_name' => 'Office Desk',
            'product_type' => 'Table',
            'product_description' => 'An Office Desk with draws in order to organise your daily office workflows',
            'product_price' => 100,
            'price_deduction' => 15.80,
            'image' => 'office desk.jpg'
        ]);

        Products::create([
            'product_name' => 'Dining Table with Four Chairs (Black)',
            'product_type' => 'Table',
            'product_description' => 'Explore the modernised dining table, a dining table with four chairs, perfect for the family.',
            'product_price' => 269,
            'price_deduction' => 25,
            'image' => 'dining table four chairs black.jpg'
        ]);

        Products::create([
            'product_name' => 'Plastic Chair',
            'product_type' => 'Chair',
            'product_description' => 'A chair that is stackable, and can be stored away easily and be re-used according to your needs',
            'product_price' => 12,
            'price_deduction' => 0,
            'image' => 'plastic chair.jpg'
        ]);

        Products::create([
            'product_name' => 'Sporda Natural Three Seat Sofa - Cream',
            'product_type' => 'Sofa',
            'product_description' => 'A sofa that has deep cusions perfect for a family. Enjoy your relaxation times.',
            'product_price' => 599,
            'price_deduction' => 149.99,
            'image' => 'cream-three-seater-sofa.jpg'
        ]);

        Products::create([
            'product_name' => 'Two Seat Sofa - Black',
            'product_type' => 'Sofa',
            'product_description' => 'A two seater sofa perfect for couples. Tailored for relaxation, even for individuals.',
            'product_price' => 439,
            'price_deduction' => 129.99,
            'image' => 'two-seater-sofa.jpg'
        ]);

        Products::create([
            'product_name' => 'Balestrand Single Bed with Cupboards',
            'product_type' => 'Bed',
            'product_description' => 'A single bed with storage, providing convenience to your hands.',
            'product_price' => 200,
            'price_deduction' => 49.99,
            'image' => 'balestrand-single-bed.jpg'
        ]);

        Products::create([
            'product_name' => 'Table with Four Chairs - Grey and White',
            'product_type' => 'Table',
            'product_description' => 'A table that is originally designed for catering 4 people, can be expanded for upto 6 people with an extender.',
            'product_price' => 399,
            'price_deduction' => 79.99,
            'image' => 'cotton-four-chairs.jpg'
        ]);

        Products::create([
            'product_name' => 'Arm Chair',
            'product_type' => 'Chair',
            'product_description' => 'A arm chair made from oak, perfect for comfort.',
            'product_price' => 30,
            'price_deduction' => 2.99,
            'image' => 'armchair beige.jpg'
        ]);

        
        Products::create([
            'product_name' => 'Green Sofa with Storage',
            'product_type' => 'Chair',
            'product_description' => 'A green chair that is made from strong textures of fabric for double comfort, along with storage.',
            'product_price' => 45,
            'price_deduction' => 6.99,
            'image' => 'green-sofa-storage.jpg'
        ]);

        Products::create([
            'product_name' => 'Circular Desktop Chair - Grey',
            'product_type' => 'Chair',
            'product_description' => 'A height adjustable desktop chair perfect for meeting all needs.',
            'product_price' => 75,
            'price_deduction' => 15.99,
            'image' => 'circular-chair.jpg'
        ]);

        Products::create([
            'product_name' => 'Three Seat Sofa Longue - Grey',
            'product_type' => 'Sofa',
            'product_description' => 'A three seater sofa combined with a longue.',
            'product_price' => 699,
            'price_deduction' => 90.99,
            'image' => 'three-seat-sofa-lounge.jpg'
        ]);

        Products::create([
            'product_name' => 'Ottoman Bed Double - Grey',
            'product_type' => 'Bed',
            'product_description' => 'An ottoman that provides the greatest comfort.',
            'product_price' => 649,
            'price_deduction' => 49.99,
            'image' => 'ottoman bed grey.jpg'
        ]);

        Products::create([
            'product_name' => 'Double Sized Bed Frame with Storage - Black',
            'product_type' => 'Bed',
            'product_description' => 'A double sized bed with frame for providing additional storage..',
            'product_price' => 579,
            'price_deduction' => 99.99,
            'image' => 'double-sized-bed-frame-black.jpg'
        ]);

        Products::create([
            'product_name' => 'Round Family table with four chairs - Oak',
            'product_type' => 'Table',
            'product_description' => 'A round family table with four chairs. For protection adhesives are used to prevent wear and tear from bottom surface.',
            'product_price' => 849,
            'price_deduction' => 249.99,
            'image' => 'round-family-table.jpg'
        ]);

    }
}
