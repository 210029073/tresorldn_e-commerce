@extends('admin.system')
<head>
    <title>Admin - Customers</title>
    <link rel="stylesheet" href="{{asset("css/pastOrders.css")}}"/>
</head>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-13">
                <div class="card">
                    <div class="card-body">
                        <h2>Displaying all Products</h2>
                        <!-- Constructs a table that holds a list of products in stock from the
                    products table -->
                        <table>
                            <tr class="attributes">
                                <td>Review ID</td>
                                <td>
                                    Reviewee's Name
                                </td>
                                <td>
                                    Reviewee's Email
                                </td>
                                <td>
                                    <div>Reviewee's Description</div>
                                </td>
                            </tr>

                            @foreach ($reviews as $item)
                                <tr class="data">
                                    <td>{{$item->id}}</td>
                                    <td><strong>{{$item->name}}</strong></td>
                                    <td>
                                        {{$item->email}}
                                    </td>
                                    <td>
                                        <i>{{$item->description}}</i>
                                    </td>
                                </tr>
                                @endforeach
                        </table>
                        <!-- Rendering mobile interface -->
                        @foreach($reviews as $item)
                            <div class="order_table">
                                <div id="order-header">
                                        <span>
                                            Review ID:
                                        </span>
                                    <br/>
                                    <span>
                                            <strong>{{$item->id}}</strong>
                                        </span>

                                </div>
                                    <div id="customer-orders">
                                        <p>Reviewee's Name: <strong>{{$item->name}}</strong></p>
                                        <p>Reviewee's E-mail: {{$item->email}}</p>
                                        <p>Reviewee's Description: {{$item->description}}</p>
                                    </div>
                            </div>
                            <br/>
                        @endforeach
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>
@endsection
