<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * This will create a review, which will be stored on the database.
     * 
     * @author Ibrahim Ahmad <210029073@aston.ac.uk>
     */
    public function createReview(Request $request) {
        // dd($_POST['personName']);
        \App\Models\Review::create([
            'name' => $_POST['personName'],
            'email' => $request->email,
            'description' => $_POST['description']
        ]);
        return redirect()->intended('review')->with('addReview', 'We would look forward from hearing from you!');
    }

    /***
     * This will count the number of views that are available
     * 
     * @author Ibrahim Ahmad <210029073@aston.ac.uk>
     */
    public function size() {
        return count(\App\Models\Review::all());
    }
}
